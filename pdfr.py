import gkeepapi
import subprocess
import os.path
from urlparse import urlparse
import urllib

def downloadFile(directory,domain,filename,address):
		#call is blocking, could use Popen instead but may not get full stderr output
		try:
			print("~Starting Chrome Headless @ "+address)
			subprocess.call(["mkdir","-p",os.path.join("./",directory,domain)])
			
			subprocess.call(["chromium-browser",
									"--headless",
									"--disable-gpu",
									"--print-to-pdf="+(os.path.join("./",directory,domain)+"/"+filename+".pdf"), 
									address
								])
		except subprocess.CalledProcessError:
			print("~Error: Skipping item")
			return False
		print("~Successfully PDfied "+filename)
		return True



keep=gkeepapi.Keep()
print("Logging in")
succ = keep.login('username','password')
if not succ:
	print("Login failed, exiting")
	exit(1)
print("Okay, lets go")
notes = list(keep.find(colors=[gkeepapi.node.ColorValue.Gray]))
print("Number of notes found: " + str(len(notes)))
print("Iterating through notes")
for note in notes:
	directory=note.title
	print("~~Title: "+directory)
	print("~~Items: "+str(len(note.items)))
	for item in note.items:
		if item.checked==True:
			continue
		try:
			print("~Found item: "+item.text)
			url=item.text   #full url (probably)
			o=urlparse(url)
			domain=o[1]	#Domain name

			page=urllib.urlopen(o.geturl()).read()

			filename=str(page).split('<title>')[1].split('</title>')[0]
			item.checked=downloadFile(directory,domain,filename,url)
		except:
			print("~Error, skipping")
			pass
keep.sync()