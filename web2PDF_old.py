import gkeepapi #pip install gkeepapi
import subprocess
import os.path
import lxml.html

funcString='web2PDF() { chromium-browser --headless --disable-gpu --print-to-pdf="$2".pdf "$1"; }'
saveLocation=None
keep=gkeepapi.Keep()
loggedIn=False
gnotes=None

def loginKeep():
    print("~Logging into keep")
    loggedIn=keep.login('b11484','enabitvdavoaaavx') #app password, not real

def getLists():
    print("~Fetching all gray notes")
    return keep.find(colors=[gkeepapi.node.ColorValue.Gray])

def downloadFile(directory,domain,filename,address):
    if saveLocation==None:
        return
    #call is blocking, could use Popen instead but may not get full stderr output
    try:
        print("~Running Chrome Headless @ "+address)
        subprocess.check_call(["chromium-browser",
                                    "--headless",
                                    "--disable-gpu",
                                    "--print-to-pdf="+os.path.join(saveLocation,directory,domain,filename)+".pdf", 
                                    address
                                ])
    except subprocess.CalledProcessError:
        print("~Error: Skipping item")
        return False
    print("~Successfully PDfied "+filename)
    return True

def fetchItem(gnote):
    print("~Beginning to parse a note")
    directory=gnote.title   #use the title of the list as the directory name
    for item in gnote.items:
        if item.checked==True:
            continue
        try:
            try:
                from urllib.parse import urlparse
            except:
                from urlparse import urlparse
            url=item.text   #full url (probably)
            parsed_uri = urlparse(url)
            domain = '{uri.netloc}/'.format(uri=parsed_uri) #Get hostname of page        
            temp = lxml.html.parse(url)
            filename=temp.find(".//title").text #create a filename based off the title
            gnote.checked=downloadFile(directory,domain,filename,url)
        except:
            pass
    keep.sync()

def standard():
    gnotes=getLists()
    for note in gnotes:
        fetchItem(note)
        

"Collect command-line options in a dictionary"""
def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

if __name__ == '__main__':
    #from sys import argv
    #myargs = getopts(argv)
    #if '-d' in myargs:  # Example usage.
        #print(myargs['-i'])
    saveLocation="./test/"
    #else:
    #    print("-d directory flag required.  Exiting....")
    #    exit(1)
    standard()